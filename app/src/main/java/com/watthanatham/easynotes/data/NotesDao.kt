package com.watthanatham.easynotes.data

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface NotesDao {
    @Insert
    suspend fun insert(notes: Notes)

    @Update
    suspend fun update(notes: Notes)

    @Delete
    suspend fun delete(notes: Notes)

    @Query("SELECT * from notes WHERE title = :title")
    fun getNote(title: String): Flow<Notes>

    @Query("SELECT * from notes ORDER BY title ASC")
    fun getNotes(): Flow<List<Notes>>
}