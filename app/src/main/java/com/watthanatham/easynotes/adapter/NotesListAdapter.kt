package com.watthanatham.easynotes.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.watthanatham.easynotes.data.Notes
import com.watthanatham.easynotes.databinding.FragmentHomeBinding

class NotesListAdapter(private val onNoteClicked: (Notes) -> Unit) : ListAdapter<Notes, NotesListAdapter.NotesViewHolder>(DiffCallback) {
    companion object {
        private val DiffCallback = object: DiffUtil.ItemCallback<Notes>() {
            override fun areItemsTheSame(oldItem: Notes, newItem: Notes): Boolean {
                return oldItem.titleName == newItem.titleName
            }
            override fun areContentsTheSame(oldItem: Notes, newItem: Notes): Boolean {
                return oldItem == newItem
            }
        }
    }

    class NotesViewHolder(private var binding: FragmentHomeBinding): RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SimpleDateFormat")
        fun bind(notes: Notes) {
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotesViewHolder {
        val viewHolder = NotesViewHolder(
            FragmentHomeBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
        viewHolder.itemView.setOnClickListener{
            val position = viewHolder.adapterPosition
            onNoteClicked(getItem(position))
        }
        return  viewHolder
    }

    override fun onBindViewHolder(holder: NotesViewHolder, position: Int) {
        val current = getItem(position)
        holder.itemView.setOnClickListener{
            onNoteClicked(current)
        }
        holder.bind(current)
    }
}