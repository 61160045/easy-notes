package com.watthanatham.easynotes

import androidx.lifecycle.*
import com.watthanatham.easynotes.data.Notes
import com.watthanatham.easynotes.data.NotesDao
import kotlinx.coroutines.launch
import java.lang.IllegalArgumentException

class NotesViewModel(private val notesDao: NotesDao): ViewModel() {
    val allNotes: LiveData<List<Notes>> = notesDao.getNotes().asLiveData()
    private fun insertNote(notes: Notes) {
        viewModelScope.launch {
            notesDao.insert(notes)
        }
    }
    private fun getNewNoteEntry(titleName: String, priority: Int, description: String, date: String): Notes {
        return Notes(
            titleName = titleName,
            priority = priority.toInt(),
            description = description,
            date = date
        )
    }
    private fun updateNote(notes: Notes) {
        viewModelScope.launch {
            notesDao.update(notes)
        }
    }
    private fun getUpdateNoteEntry(
        noteId: Int,
        titleName: String,
        priority: Int,
        description: String,
        date: String
    ): Notes {
        return Notes(
            id = noteId,
            titleName = titleName,
            priority = priority.toInt(),
            description = description,
            date = date
        )
    }
    fun addNewNote(titleName: String, priority: Int, description: String, date: String) {
        val newNote = getNewNoteEntry(titleName, priority, description, date)
        insertNote(newNote)
    }
    fun updateNote(noteId: Int, titleName: String, priority: Int, description: String, date: String) {
        val updatedNote = getUpdateNoteEntry(noteId, titleName, priority, description, date)
        updateNote(updatedNote)
    }
    fun deleteNote(notes: Notes) {
        viewModelScope.launch {
            notesDao.delete(notes)
        }
    }
}
class NotesViewModelFactory(private val notesDao: NotesDao) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NotesViewModel::class.java)) {
            return NotesViewModel(notesDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}